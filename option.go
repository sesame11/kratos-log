package kratos_log

type Option func(config *FileWriteSyncConfig)

func FileName(fileName string) Option {
	return func(config *FileWriteSyncConfig) {
		config.fileName = fileName
	}
}

func MaxSize(maxSize int) Option {
	return func(config *FileWriteSyncConfig) {
		config.maxSize = maxSize
	}
}

func MaxBackup(maxBackup int) Option {
	return func(config *FileWriteSyncConfig) {
		config.maxBackup = maxBackup
	}
}

func MaxAge(maxAge int) Option {
	return func(config *FileWriteSyncConfig) {
		config.maxAge = maxAge
	}
}

func Compress(compress bool) Option {
	return func(config *FileWriteSyncConfig) {
		config.compress = compress
	}
}
