module gitlab.com/sesame11/kratos-log

go 1.18

require (
	github.com/go-kratos/kratos/contrib/log/zap/v2 v2.0.0-20220915133113-7f2666be9f86
	go.uber.org/zap v1.23.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

require (
	github.com/go-kratos/kratos/v2 v2.5.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
