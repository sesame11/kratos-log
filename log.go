package kratos_log

import (
	zapv2 "github.com/go-kratos/kratos/contrib/log/zap/v2"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
)

type FileWriteSyncConfig struct {
	fileName  string
	maxSize   int
	maxBackup int
	maxAge    int
	compress  bool
}

func NewZapLogger(options ...Option) *zapv2.Logger {
	fileWriteSyncConfig := &FileWriteSyncConfig{
		fileName:  "/tmp/log.log",
		maxSize:   100,
		maxBackup: 5,
		maxAge:    30,
		compress:  false,
	}

	if len(options) > 0 {
		for _, option := range options {
			option(fileWriteSyncConfig)
		}
	}
	return zapv2.NewLogger(zap.New(getNewCore(fileWriteSyncConfig), zap.AddCaller()))

}

func getNewCore(fileWriteSyncConfig *FileWriteSyncConfig) zapcore.Core {
	return zapcore.NewCore(getEncoder(), getWriteSync(fileWriteSyncConfig), zapcore.WarnLevel)
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	encoderConfig.MessageKey = ""
	return zapcore.NewJSONEncoder(encoderConfig)
}

func getWriteSync(fileWriteSyncConfig *FileWriteSyncConfig) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   fileWriteSyncConfig.fileName,
		MaxSize:    fileWriteSyncConfig.maxSize,
		MaxBackups: fileWriteSyncConfig.maxBackup,
		MaxAge:     fileWriteSyncConfig.maxAge,
		Compress:   fileWriteSyncConfig.compress,
	}
	return zapcore.NewMultiWriteSyncer(zapcore.AddSync(lumberJackLogger), zapcore.AddSync(os.Stdout))
}
